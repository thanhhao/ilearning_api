const db = require('./index');
const Sequelize = require('sequelize');

const HistoryQuiz = db.define('history-quiz',{
    id: { autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER},
    quizId: { type: Sequelize.INTEGER},
    time_created: { type: Sequelize.STRING,notEmpty: true},
    time_do_quiz: { type: Sequelize.STRING,notEmpty: true},
    grade: { type: Sequelize.FLOAT ,notEmpty: true},
    detail_result: { type: Sequelize.JSON ,notEmpty: true},
})
// Forum.belongsTo(Admin);
module.exports = HistoryQuiz;
