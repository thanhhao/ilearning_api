const db = require('./index');
const Sequelize = require('sequelize');
const Course = require("./course");
const User = require("./user");

const Quiz = db.define('quiz',{
    id: { autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER},
    name: { type: Sequelize.STRING,notEmpty: true},
    subject: { type: Sequelize.STRING,notEmpty: true},
    description: { type: Sequelize.STRING},
    list_question: { type: Sequelize.STRING,notEmpty: true}
})
Quiz.belongsTo(Course);

module.exports = Quiz;
