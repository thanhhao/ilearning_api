var exports = module.exports = {};
const HistoryQuiz = require("../models/history_quiz")

exports.createHistoryQuiz = (req,res,next) => {
    HistoryQuiz.findAll({ where: { id: 1 } })
        .then(courses=>{
            const historyQuiz = new HistoryQuiz({
                quizId: req.body.quizId,
                time_created: req.body.time_created,
                time_do_quiz: req.body.time_do_quiz,
                grade: req.body.grade,
                detail_result: req.body.detail_result,
            });
            historyQuiz
                .save()
                .then(result => {
                    res.status(200).json({
                        isSuccessfully: true,
                        message: "History quiz created"
                    });
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).json({
                        isSuccessfully: false,
                        error: err
                    });
                });
        }).catch(error=>{
        console.log(err);
        res.status(500).json({
            isSuccessfully: false,
            error: err
        });
    });
};

exports.getHistoryQuiz = (req,res,next) =>{
    HistoryQuiz.findAll({})
        .then(history=>{
            // from = idPage*numShowPerPage;
            // to = Math.min((idPage+1)*(numShowPerPage),courses.length);
            let historyList = [];
            for(let i=0;i<history.length;i++){
                historyList.push({
                    id: history[i].id,
                    quizId: history[i].quizId,
                    time_created: history[i].time_created,
                    time_do_quiz: history[i].time_do_quiz,
                    grade: history[i].grade,
                    detail_result: history[i].detail_result,
                })
            }
            // historyList = historyList.slice(from,to);
            if(historyList.length>0){
                return res.status(200).json({
                    sSuccessfully: true,
                    historyList: historyList,
                })
            } else {
                return res.status(200).json({
                    isSuccessfully: false,
                    message: "request failed"
                });
            }
        }).catch(error=>{
        console.log(err);
        res.status(500).json({
            isSuccessfully: false,
            error: err
        });
    });
};

