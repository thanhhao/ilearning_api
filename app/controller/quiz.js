var exports = module.exports = {};
const Quiz = require("../models/quiz");
const Course = require("../models/course");

exports.getAllQuizWithCourseId = (req, res, next) => {
    let courseId = req.body.courseId;
    let idAdmin = req.userData.userId;
    Course.findAll({where: {id: courseId}})
        .then(courses => {
            if (courses.length !== 1) {
                return res.status(200).json({
                    isSuccessfully: false,
                    message: "course is not exist"
                });
            }
            let course = courses[0];
            if (course.adminId !== idAdmin) {
                return res.status(200).json({
                    isSuccessfully: false,
                    message: "auth fail"
                });
            }
            Quiz.findAll({where: {courseId: course.id}})
                .then(quizs => {
                    listQuiz = [];
                    for (i = 0; i < quizs.length; i++) {
                        quiz = quizs[i];
                        dataQuiz = {
                            courseId: quiz.courseId,
                            id: quiz.id,
                            name: quiz.name,
                            subject: quiz.subject,
                            description: quiz.description,
                            list_question: quiz.list_question,
                        }
                        listQuiz.push(dataQuiz)
                    }
                    return res.status(200).json({
                        isSuccessfully: true,
                        listQuiz: listQuiz,
                        message: "succcessfully"
                    });
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).json({
                        isSuccessfully: false,
                        error: err
                    })
                })
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                isSuccessfully: false,
                error: err
            });
        })
};

exports.getQuizWithId = (req, res, next) => {
    quizId = req.body.quizId;
    courseId = req.body.courseId;
    idAdmin = req.userData.userId;
    Quiz.findAll({where: {id: quizId}})
        .then(quiz => {
            dataQuiz = {
                courseId: quiz.courseId,
                id: quiz.id,
                name: quiz.name,
                subject: quiz.subject,
                description: quiz.description,
                list_question: quiz.list_question,
            }

            return res.status(200).json({
                isSuccessfully: true,
                quiz: dataQuiz,
                message: "succcessfully"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                isSuccessfully: false,
                error: err
            })
        })
};

exports.createQuiz = (req, res, next) => {
    let courseId = req.body.courseId;
    let adminId = req.userData.userId;
    Course.findAll({where: {id: courseId}}).then(
        courses => {
            if (courses.length !== 1) {
                return res.json({
                    isSuccessfully: false,
                    message: "Course is not exist!"
                })
            }
            let course = courses[0];
            const quiz = new Quiz({
                courseId: courseId,
                name: req.body.name,
                subject: req.body.subject,
                list_question: req.body.list_question,
                description: req.body.description,
            });
            quiz.save().then(
                result => {
                    return res.status(200).json({
                        isSuccessfully: true,
                        quiz: quiz.toJSON()
                    })
                }
            ).catch(err => {
                console.log(err);
                res.status(500).json({
                    isSuccessfully: false,
                    error: err
                });
            });
        }
    ).catch(err => {
        console.log(err);
        res.status(500).json({
            isSuccessfully: false,
            error: err
        });
    })
}

exports.updateQuiz = (req, res, next) => {
    let courseId = req.body.courseId;
    let idAdmin = req.userData.userId;
    let quizId = req.body.quizId;
    Course.findAll({where: {id: courseId}})
        .then(courses => {
            if (courses.length !== 1) {
                return res.status(200).json({
                    isSuccessfully: false,
                    message: "course is not exist"
                });
            }
            course = courses[0]
            if (course.adminId !== idAdmin) {
                return res.status(200).json({
                    isSuccessfully: false,
                    message: "auth fail"
                });
            }

            Quiz.findAll({where: {id: quizId}})
                .then(quiz => {
                    quiz.courseId = courseId;
                    quiz.name = req.body.name;
                    quiz.subject = req.body.subject;
                    quiz.list_question = req.body.list_question;
                    quiz.description = req.body.description;
                    quiz
                        .save()
                        .then(result => {
                            res.status(200).json({
                                isSuccessfully: true,
                                message: "Quiz update"
                            });
                        })
                        .catch(err => {
                            console.log(err);
                            res.status(500).json({
                                isSuccessfully: false,
                                error: err
                            });
                        });
                })
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                isSuccessfully: false,
                error: err
            });
        })
};

