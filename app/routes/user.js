const router = require('express').Router();
const userController = require("../controller/user");
const checkAuthUser = require('../middleware/user-auth');
const quizController = require("../controller/quiz");
const historyQuizController = require('../controller/history-quiz')

router.get("/info/:userId",checkAuthUser,userController.getInfoUser);
router.post("/getQuizWithId",checkAuthUser,quizController.getQuizWithId);
router.post("/signCourse",checkAuthUser, userController.userSignCourse);
router.post("/login", userController.checkValidUser);
router.post("/signup", userController.createUser);
router.get('/logout',checkAuthUser,(req,res,next)=>{
    delete req.headers.authorization;
    res.status(200).send({
        isSuccessfully: false,
        message: "logout successful",
    });
})
router.post('/historyQuiz', historyQuizController.createHistoryQuiz)
router.get('/historyQuiz', historyQuizController.getHistoryQuiz)

router.post('/private',checkAuthUser,(req,res,next)=>{
    res.status(200).send('ok');
})

module.exports = router;
